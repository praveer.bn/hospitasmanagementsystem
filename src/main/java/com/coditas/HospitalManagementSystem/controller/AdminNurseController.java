package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.service.interfaces.NurseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
@CrossOrigin
@RestController
//@PreAuthorize("hasRole('ADMIN')")
@RequestMapping("/api/admin/nurse")
public class AdminNurseController {
    @Autowired
    private NurseService nurseService;
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public List<NurseDto> getNurseList(){
        List<NurseDto> nurseDtoList= nurseService.getNurseList();
        return nurseDtoList;
    }
//    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/available")
    public List<NurseDto> getAvailableNurseList(){
        List<NurseDto> nurseDtoList= nurseService.getAvailableNurseList();
        return nurseDtoList;
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<HashMap<NurseDto, String>> addNurse(@RequestBody NurseDto nurseDto){
        HashMap<NurseDto, String> map = new HashMap<>();
        NurseDto newNurseDto = nurseService.addNurse(nurseDto);
        if (newNurseDto == null) {
            map.put(new NurseDto(), "Could Not Process The Request....Error Occurred While Inserting Nurse");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newNurseDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }
    @CrossOrigin
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{nurseId}")
    public ResponseEntity<HashMap<NurseDto, String>> updateNurse(@PathVariable long nurseId, @RequestBody NurseDto nurseDto){
        HashMap<NurseDto, String> map = new HashMap<>();
        NurseDto newNurseDto = nurseService.updateNurse(nurseDto,nurseId);
        if (newNurseDto == null) {
            map.put(new NurseDto(), "Could Not Process The Request....Error Occurred While Updating Nurse");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newNurseDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }


    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/assign")
    public ResponseEntity<HashMap<NurseDto, String>> assignDoctor(@RequestBody AssignDto assignDto){
        HashMap<NurseDto, String> map = new HashMap<>();
        NurseDto newNurseDto =  nurseService.allocateDoctor(assignDto);
        if (newNurseDto == null) {
            map.put(new NurseDto(), "Could Not Process The Request....Error Occurred While Assigning Doctor To Nurse");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newNurseDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }

    @CrossOrigin
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{nurseId}")
    public ResponseEntity<HashMap<String,String>> deleteNurse(@PathVariable long nurseId) {
        nurseService.deleteNurse(nurseId);
        HashMap<String, String> map = new HashMap<>();
        map.put("Nurse Details Deleted....", null);
        return new ResponseEntity<>(map, HttpStatus.OK);
        }

    }
