package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.entity.User;
import com.coditas.HospitalManagementSystem.payload.ForgetDto;
import com.coditas.HospitalManagementSystem.payload.JwtAuthResponse;
import com.coditas.HospitalManagementSystem.payload.LoginDto;
import com.coditas.HospitalManagementSystem.payload.ResetDto;
import com.coditas.HospitalManagementSystem.repository.RoleRepository;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import com.coditas.HospitalManagementSystem.security.JwtTokenProvider;
import com.coditas.HospitalManagementSystem.service.impl.NurseServiceImpl;
import com.coditas.HospitalManagementSystem.service.interfaces.PasswordService;
import com.coditas.HospitalManagementSystem.utils.EmailSenderService;
import com.coditas.HospitalManagementSystem.utils.PasswordEncoderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private PasswordService passwordService;


    @CrossOrigin
    @PostMapping("/signin")
    public ResponseEntity<JwtAuthResponse> authenticateUser(@RequestBody LoginDto loginDto){
        Authentication authentication= authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUserNameOrEmail(),loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //get toke fro token provider
        String token=jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthResponse(token,jwtTokenProvider.getRole(jwtTokenProvider.getUsername(token)), jwtTokenProvider.getUserId(jwtTokenProvider.getUsername(token))));
    }
    @PostMapping("/forgot")
    public ResponseEntity<String> forgetPassWord(@RequestBody ForgetDto forgetDto){
      return  new ResponseEntity<>(passwordService.forgotPassWord(forgetDto.getUserNameOrEmail()),HttpStatus.OK);
    }
    @PostMapping("/reset")
    public ResponseEntity<String> resetPassWord(@RequestBody ResetDto resetDto){
        return  new ResponseEntity<>(passwordService.resetPassword(resetDto.getUserNameOrEmail(),resetDto.getNewPassword(),resetDto.getOldPassword()),HttpStatus.OK);
    }

}
