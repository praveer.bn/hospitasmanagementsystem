package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.payload.*;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorRequestService;
import com.coditas.HospitalManagementSystem.service.interfaces.NurseRequestService;
import com.coditas.HospitalManagementSystem.service.interfaces.NurseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@PreAuthorize("hasRole('NURSE')")
@RequestMapping("/api/nurse")
public class NurseController {
    @Autowired
    private NurseRequestService nurseRequestService;
    @Autowired
    private NurseService nurseService;
    @PostMapping("/replace")
    public ResponseEntity<NurseRequestDto> doReplaceRequest(@RequestBody NurseRequestDto nurseRequestDto){

        NurseRequestDto nurseRequestDto1=nurseRequestService.doReplaceRequest(nurseRequestDto);
        if(nurseRequestDto1!=null) {
            return new ResponseEntity<>(nurseRequestDto1, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/history")
    public ResponseEntity<List<NurseRequestDto>> getAllNurseRequestHistory(){
        return new ResponseEntity<>(nurseRequestService.getAllRequestHistory(), HttpStatus.CREATED);
    }

    @GetMapping("/history/{nurseId}")
    public ResponseEntity<List<NurseRequestDto>> getSpecificNurseRequestHistory(@PathVariable long nurseId){
        return new ResponseEntity<>(nurseRequestService.getSpecificNurseRequestHistory(nurseId), HttpStatus.CREATED);

    }

    @GetMapping("/{nurseId}")
    public ResponseEntity<NurseDto> getSpecificDoctor(@PathVariable long nurseId){
        return new ResponseEntity<>(nurseService.getSpecificNurse(nurseId), HttpStatus.CREATED);
    }
    @GetMapping("availableDoctor/{nurseId}")
    public ResponseEntity<List<DoctorDto>> getAllAvailableReplacingDoctor(@PathVariable long nurseId){
        return new ResponseEntity<>(nurseRequestService.getAllAvailableReplacingDoctor(nurseId), HttpStatus.CREATED);
    }




}
