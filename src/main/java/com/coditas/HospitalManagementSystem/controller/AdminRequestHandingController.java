package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.payload.*;
import com.coditas.HospitalManagementSystem.service.impl.AdminRequestServiceImpl;
import com.coditas.HospitalManagementSystem.service.interfaces.AdminRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin
@RestController
@PreAuthorize("hasRole('ADMIN')")
@RequestMapping("/api/admin/request")
public class AdminRequestHandingController {
    @Autowired
    private AdminRequestService adminRequestService;

    @GetMapping("/doctor/pendingRequest")
    public ResponseEntity<List<DoctorRequestDto>> getPendingDoctorRequestHistory(){
        return new ResponseEntity<>(adminRequestService.getDoctorPendingRequestHistory(), HttpStatus.CREATED);

    }

    @GetMapping("/nurse/pendingRequest")
    public ResponseEntity<List<NurseRequestDto>> getPendingNurseRequestHistory(){
        return new ResponseEntity<>(adminRequestService.getNursePendingRequestHistory(), HttpStatus.CREATED);
    }

    @PostMapping("/nurse")
    public ResponseEntity<HashMap<RequestDto,String>> nurseRequestHandling(@RequestBody RequestDto requestDto){
        HashMap<RequestDto, String> map = new HashMap<>();
        RequestDto requestDto1=adminRequestService.nurseRequestHandling(requestDto);
        if(requestDto1==null){
            map.put(null, "Could Not Process The Request....");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(requestDto1, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @PostMapping("/doctor")
    public ResponseEntity<HashMap<RequestDto,String>> doctorRequestHandling(@RequestBody RequestDto requestDto){
        HashMap<RequestDto, String> map = new HashMap<>();
        RequestDto requestDto1=adminRequestService.doctorRequestHandling(requestDto);
        if(requestDto1==null){
            map.put(null, "Could Not Process The Request....");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(requestDto1, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }


}
