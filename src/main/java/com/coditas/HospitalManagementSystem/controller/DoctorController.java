package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.payload.NurseRequestDto;
import com.coditas.HospitalManagementSystem.service.impl.AdminRequestServiceImpl;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorRequestService;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@PreAuthorize("hasRole('DOCTOR')")
@RequestMapping("/api/doctor")
public class DoctorController {
    @Autowired
    private DoctorRequestService doctorRequestService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private AdminRequestServiceImpl adminRequestService;
    @PostMapping("/replace")
    public ResponseEntity<DoctorRequestDto> doReplaceRequest(@RequestBody DoctorRequestDto doctorRequestDto){
        DoctorRequestDto doctorRequestDto1=doctorRequestService.doReplaceRequest(doctorRequestDto);
        if(doctorRequestDto1!=null) {
            return new ResponseEntity<>(doctorRequestDto, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/history")
    public ResponseEntity<List<DoctorRequestDto>> getAllDoctorRequestHistory(){
        return new ResponseEntity<>(doctorRequestService.getAllRequestHistory(), HttpStatus.CREATED);
    }

    @GetMapping("/history/{doctorId}")
    public ResponseEntity<List<DoctorRequestDto>> getSpecificDoctorRequestHistory(@PathVariable long doctorId){
        return new ResponseEntity<>(doctorRequestService.getSpecificDoctorRequestHistory(doctorId), HttpStatus.CREATED);
    }
    @GetMapping("/{doctorId}")
    public ResponseEntity<DoctorDto> getSpecificDoctor(@PathVariable long doctorId){
        return new ResponseEntity<>(doctorService.getSpecificDoctor(doctorId), HttpStatus.CREATED);

    }

    @GetMapping("/nurseSet/{doctorId}")
    public ResponseEntity<Set<NurseDto>> getNurseForRespectedDoctor(@PathVariable long doctorId){
        return new ResponseEntity<>(adminRequestService.getNurseForRespectedDoctor(doctorId), HttpStatus.CREATED);

    }



}
