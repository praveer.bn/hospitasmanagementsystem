package com.coditas.HospitalManagementSystem.controller;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
//@PreAuthorize("hasRole('ADMIN')")
@RequestMapping("/api/admin/doctor")
public class AdminDoctorController {
    @Autowired
   private DoctorService doctorService;

    //DOCTOR
//    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public List<DoctorDto> getDoctorList(){
       List<DoctorDto> doctorDtoList= doctorService.getDoctorList();
       return doctorDtoList;
    }
//
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<HashMap<DoctorDto, String>> addDoctor(@RequestBody DoctorDto doctorDto){
        HashMap<DoctorDto, String> map = new HashMap<>();
        DoctorDto newDoctorDto = doctorService.addDoctor(doctorDto);
        if (newDoctorDto == null) {
            map.put(new DoctorDto(), "Could Not Process The Request....Error Occurred While Inserting Doctor.");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newDoctorDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{doctorId}")
    public ResponseEntity<HashMap<DoctorDto, String>> updateDoctor(@PathVariable long doctorId,@RequestBody DoctorDto doctorDto){
        HashMap<DoctorDto, String> map = new HashMap<>();
        DoctorDto newDoctorDto = doctorService.updateDoctor(doctorDto,doctorId);
        if (newDoctorDto == null) {
            map.put(new DoctorDto(), "Could Not Process The Request....Error Occurred While Updating Doctor.");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newDoctorDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{doctorId}")
    public ResponseEntity<HashMap<String,String>> deleteDoctor(@PathVariable long doctorId){
        doctorService.deleteDoctor(doctorId);
        HashMap<String, String> map = new HashMap<>();
        map.put("Doctor deleted successfully",null);
        return  new ResponseEntity<>(map,HttpStatus.OK);
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/assign")
    public ResponseEntity<HashMap<DoctorDto, String>> assignNurse(@RequestBody AssignDto assignDto){
        HashMap<DoctorDto, String> map = new HashMap<>();
        DoctorDto newDoctorDto = doctorService.allocateNurse(assignDto);
        if (newDoctorDto == null) {
            map.put(new DoctorDto(), "Could Not Process The Request....Error Occurred While Assigning Nurse To Doctor");
            return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
        }
        map.put(newDoctorDto, null);
        return new ResponseEntity<>(map, HttpStatus.CREATED);

    }




}
