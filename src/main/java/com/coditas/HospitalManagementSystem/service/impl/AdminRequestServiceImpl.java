package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.DoctorRequest;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.entity.NurseRequest;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.payload.NurseRequestDto;
import com.coditas.HospitalManagementSystem.payload.RequestDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.DoctorRequestRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRequestRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.AdminRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Service
public class AdminRequestServiceImpl implements AdminRequestService {
    @Autowired
    private DoctorRequestRepository doctorRequestRepository;
    @Autowired
    private NurseRequestRepository nurseRequestRepository;
    @Autowired
    private NurseRepository nurseRepository;
    @Autowired
    private NurseServiceImpl nurseService;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private DoctorRequestServiceImpl doctorRequestServiceImpl;
    @Autowired
    private NurseRequestServiceImpl nurseRequestServiceImpl;
    @Override
    public RequestDto nurseRequestHandling(RequestDto requestDto) {
        System.out.println(requestDto.getRequestId());
        NurseRequest nurseRequest=nurseRequestRepository.findById(requestDto.getRequestId()).get();
        if(requestDto.getUpdatedStatus().equals("approved")){
            nurseRequest.setRequestStatus(requestDto.getUpdatedStatus());
            Nurse newNurse=nurseRepository.findById(nurseRequest.getNurse().getNurseId()).get();
            Doctor doctor=doctorRepository.findById(nurseRequest.getNewDoctorId()).get();
            newNurse.setDoctor(doctor);
            nurseRepository.save(newNurse);
            nurseRequestRepository.save(nurseRequest);

        }else{
            nurseRequest.setRequestStatus(requestDto.getUpdatedStatus());
            nurseRequest.setAdminReason(requestDto.getReason());
            nurseRequestRepository.save(nurseRequest);
        }
        return requestDto;
    }

    @Override
    public RequestDto doctorRequestHandling(RequestDto requestDto) {
        System.out.println(requestDto.getRequestId());
        DoctorRequest doctorRequest =doctorRequestRepository.findById(requestDto.getRequestId()).get();
        if(requestDto.getUpdatedStatus().equals("approved")){
            doctorRequest.setRequestStatus(requestDto.getUpdatedStatus());
            Nurse newNurse=nurseRepository.findById(doctorRequest.getNewNurseId()).get();
            newNurse.setStatus("Allocated");
            Doctor doctor=doctorRepository.findById(doctorRequest.getDoctor().getDoctorId()).get();
            newNurse.setDoctor(doctor);
            nurseRepository.save(newNurse);
            if(doctorRequest.getOldNurseId()!=0l) {
                Nurse oldNurse = nurseRepository.findById(doctorRequest.getOldNurseId()).get();
                oldNurse.setStatus("notAllocated");
                oldNurse.setDoctor(null);
                nurseRepository.save(oldNurse);
                doctorRequestRepository.save(doctorRequest);
            }

        }else{
            doctorRequest.setRequestStatus(requestDto.getUpdatedStatus());
            doctorRequest.setAdminReason(requestDto.getReason());
            doctorRequestRepository.save(doctorRequest);

        }
        return requestDto;
    }

    @Override
    public List<DoctorRequestDto> getDoctorPendingRequestHistory() {
        List<DoctorRequest> doctorRequestList=doctorRequestRepository.findAll();
        List<DoctorRequest> doctorRequestList1=doctorRequestList.stream().filter(s->s.getRequestStatus().equals("pending")).collect(Collectors.toList());
        List<DoctorRequestDto> doctorRequestDtos=doctorRequestList1.stream().map(dr->doctorRequestServiceImpl.mapToDto(dr)).collect(Collectors.toList());
        return doctorRequestDtos;
    }

    @Override
    public List<NurseRequestDto> getNursePendingRequestHistory() {
        List<NurseRequest> nurseRequestList=nurseRequestRepository.findAll();
        List<NurseRequest> nurseRequestList1=nurseRequestList.stream().filter(s->s.getRequestStatus().equals("pending")).collect(Collectors.toList());
        List<NurseRequestDto> nurseRequestDtos=nurseRequestList1.stream().map(nr->nurseRequestServiceImpl.mapToDto(nr)).collect(Collectors.toList());
        return nurseRequestDtos;
    }

    @Override
    public Set<NurseDto> getNurseForRespectedDoctor(long doctorId) {
        Doctor doctor=doctorRepository.findById(doctorId).get();
        Set<NurseDto> nurseDtoList=doctor.getNurseSet().stream().map(s->nurseService.mapToDto(s)).collect(Collectors.toSet());
        return nurseDtoList;
    }
}
