package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.DoctorRequest;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.DoctorRequestRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorRequestServiceImpl implements DoctorRequestService {
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private DoctorRequestRepository doctorRequestRepository;
    @Autowired
    private DoctorServiceImpl doctorServiceimpl;
    @Autowired
    private NurseRepository nurseRepository;
    @Override
    public List<DoctorRequestDto> getAllRequestHistory() {
        List<DoctorRequest> doctorRequestList=doctorRequestRepository.findAll();
        List<DoctorRequest> doctorRequestList1=doctorRequestList.stream().filter(s->!s.getRequestStatus().equals("pending")).collect(Collectors.toList());
        List<DoctorRequestDto> doctorRequestDtos=doctorRequestList1.stream().map(dr->mapToDto(dr)).collect(Collectors.toList());
        return doctorRequestDtos;

    }

    @Override
    public List<DoctorRequestDto> getSpecificDoctorRequestHistory(long doctorId) {
        List<DoctorRequest> doctorRequestList=doctorRequestRepository.findAll();
        List<DoctorRequest> specificDoctorRequestList=doctorRequestList.stream().filter(s->s.getDoctor().getDoctorId()==doctorId).collect(Collectors.toList());
        List<DoctorRequestDto> doctorRequestDtos=specificDoctorRequestList.stream().map(dr->mapToDto(dr)).collect(Collectors.toList());
        return doctorRequestDtos;
    }

    @Override
    public DoctorRequestDto doReplaceRequest(DoctorRequestDto doctorRequestDto) {
        DoctorRequest doctorRequest=mapToEntity(doctorRequestDto);
        if(doctorRequestDto.getOldNurseId()==0){
            List<Nurse> nurse = nurseRepository.findAll().stream().filter(s -> s.getNurseId() == doctorRequestDto.getOldNurseId()||s.getNurseId()==doctorRequestDto.getNewNurseId()).collect(Collectors.toList());
            if(nurse.size()==1) {
                DoctorRequest doctorRequest1 = doctorRequestRepository.save(doctorRequest);
                DoctorRequestDto doctorRequestDto1 = mapToDto(doctorRequest1);
                return doctorRequestDto1;
            }
            return null;
        }else {
            List<Nurse> nurse = nurseRepository.findAll().stream().filter(s -> s.getNurseId() == doctorRequestDto.getOldNurseId()||s.getNurseId()==doctorRequestDto.getNewNurseId()).collect(Collectors.toList());
            if (nurse.size() == 2) {
                DoctorRequest doctorRequest1=doctorRequestRepository.save(doctorRequest);
                DoctorRequestDto doctorRequestDto1 = mapToDto(doctorRequest1);
                return doctorRequestDto1;
            }
            return null;
        }
    }

    public DoctorRequestDto mapToDto(DoctorRequest doctorRequest){
        DoctorRequestDto doctorRequestDto=new DoctorRequestDto();
        doctorRequestDto.setDoctorReason(doctorRequest.getDoctorReason());
        doctorRequestDto.setRequestStatus(doctorRequest.getRequestStatus());
        doctorRequestDto.setRequestId(doctorRequest.getRequestId());
        doctorRequestDto.setAdminReason(doctorRequest.getAdminReason());

        doctorRequestDto.setNewNurseId(doctorRequest.getNewNurseId());
        Nurse newNurse=nurseRepository.findById(doctorRequest.getNewNurseId()).get();
        doctorRequestDto.setNewNurseName(newNurse.getNurseName());
     if(doctorRequest.getOldNurseId()!=0l) {
    doctorRequestDto.setOldNurseId(doctorRequest.getOldNurseId());
    Nurse oldNurse = nurseRepository.findById(doctorRequest.getOldNurseId()).get();
    doctorRequestDto.setOldNurseName(oldNurse.getNurseName());
     }

        doctorRequestDto.setDoctorId(doctorRequest.getDoctor().getDoctorId());
        doctorRequestDto.setDoctorName(doctorRequest.getDoctor().getDoctorName());

        return doctorRequestDto;
    }
    private DoctorRequest mapToEntity(DoctorRequestDto doctorRequestDto){
        DoctorRequest doctorRequest=new DoctorRequest();
        doctorRequest.setDoctorReason(doctorRequestDto.getDoctorReason());
        doctorRequest.setAdminReason(doctorRequestDto.getAdminReason());
        doctorRequest.setNewNurseId(doctorRequestDto.getNewNurseId());
        doctorRequest.setOldNurseId(doctorRequestDto.getOldNurseId());
        doctorRequest.setRequestId(doctorRequestDto.getRequestId());

        Doctor doctor=doctorRepository.findById(doctorRequestDto.getDoctorId()).get();
        doctorRequest.setDoctor(doctor);

        return doctorRequest;
    }
}

