package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.DoctorRequest;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.entity.NurseRequest;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.payload.NurseRequestDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.DoctorRequestRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRequestRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.NurseRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NurseRequestServiceImpl implements NurseRequestService {

    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private NurseRequestRepository nurseRequestRepository;
    @Autowired
    private NurseServiceImpl nurseServiceImpl;
    @Autowired
    private NurseRepository nurseRepository;
    @Autowired
    private DoctorServiceImpl doctorServiceImpl;
    @Override
    public List<NurseRequestDto> getAllRequestHistory() {
        List<NurseRequest> nurseRequestList=nurseRequestRepository.findAll();
        List<NurseRequest> nurseRequestList1=nurseRequestList.stream().filter(s->!s.getRequestStatus().equals("pending")).collect(Collectors.toList());
        List<NurseRequestDto> nurseRequestDtos=nurseRequestList1.stream().map(nr->mapToDto(nr)).collect(Collectors.toList());
        return nurseRequestDtos;


    }

    @Override
    public List<DoctorDto> getAllAvailableReplacingDoctor(long nurseId) {
        List<Doctor> doctorList=doctorRepository.findAll();
        Nurse nurse=nurseRepository.findById(nurseId).get();
        if(nurse.getDoctor()!=null){
            List<Doctor> availableDoctorList=doctorList.stream().filter(s->s.getDoctorId()!=nurse.getDoctor().getDoctorId()).collect(Collectors.toList());
            List<DoctorDto> doctorDtoList=availableDoctorList.stream().filter(s->s.isDeleted()==false).map(s->doctorServiceImpl.mapToDto(s)).collect(Collectors.toList());
            return doctorDtoList;
        }
//        List<Doctor> availableDoctorList=doctorList.stream().filter(s->s.getDoctorId()!=nurse.getDoctor().getDoctorId()).collect(Collectors.toList());
        List<DoctorDto> doctorDtoList=doctorList.stream().filter(s->s.isDeleted()==false).map(s->doctorServiceImpl.mapToDto(s)).collect(Collectors.toList());
        return doctorDtoList;
    }

    @Override
    public List<NurseRequestDto> getSpecificNurseRequestHistory(long nurseId) {
        List<NurseRequest> nurseRequestList=nurseRequestRepository.findAll();
        List<NurseRequest> specificNurseRequestList=nurseRequestList.stream().filter(s->s.getNurse().getNurseId()==nurseId).collect(Collectors.toList());
        List<NurseRequestDto> nurseRequestDtos=specificNurseRequestList.stream().map(nr->mapToDto(nr)).collect(Collectors.toList());
        return nurseRequestDtos;
    }

    @Override
    public NurseRequestDto doReplaceRequest(NurseRequestDto nurseRequestDto) {
        NurseRequest nurseRequest=mapToEntity(nurseRequestDto);

        if(nurseRequestDto.getOldDoctorId()==0){
            List<Doctor> doctorList = doctorRepository.findAll().stream().filter(s -> s.getDoctorId() == nurseRequestDto.getNewDoctorId()).collect(Collectors.toList());
            if(doctorList.size()==1) {
                NurseRequest nurseRequest1=nurseRequestRepository.save(nurseRequest);
                NurseRequestDto nurseRequestDto1=mapToDto(nurseRequest1);
                return nurseRequestDto1;
            }
            return null;
        }else {
            List<Doctor> doctorList = doctorRepository.findAll().stream().filter(s -> s.getDoctorId() == nurseRequestDto.getNewDoctorId()||s.getDoctorId()==nurseRequestDto.getOldDoctorId()).collect(Collectors.toList());
            if (doctorList.size() == 2) {
                NurseRequest nurseRequest1=nurseRequestRepository.save(nurseRequest);
                NurseRequestDto nurseRequestDto1=mapToDto(nurseRequest1);
                return nurseRequestDto1;
            }
            return null;
        }



    }

    public NurseRequestDto mapToDto(NurseRequest nurseRequest){
        NurseRequestDto nurseRequestDto=new NurseRequestDto();
        nurseRequestDto.setNurseReason(nurseRequest.getNurseReason());
        nurseRequestDto.setRequestStatus(nurseRequest.getRequestStatus());
        nurseRequestDto.setRequestId(nurseRequest.getRequestId());
        nurseRequestDto.setAdminReason(nurseRequest.getAdminReason());

        nurseRequestDto.setNewDoctorId(nurseRequest.getNewDoctorId());
        Doctor newDoctor=doctorRepository.findById(nurseRequest.getNewDoctorId()).get();
        nurseRequestDto.setNewDoctorName(newDoctor.getDoctorName());

        nurseRequestDto.setOldDoctorId(nurseRequest.getOldDoctorId());
        Doctor oldDoctor=doctorRepository.findById(nurseRequest.getOldDoctorId()).get();
        nurseRequestDto.setOldDoctorName(oldDoctor.getDoctorName());

        nurseRequestDto.setNurseId(nurseRequest.getNurse().getNurseId());
        nurseRequestDto.setNurseName(nurseRequest.getNurse().getNurseName());

        return nurseRequestDto;
    }

    private NurseRequest mapToEntity(NurseRequestDto nurseRequestDto){
        NurseRequest nurseRequest=new NurseRequest();
        nurseRequest.setNurseReason(nurseRequestDto.getNurseReason());
        nurseRequest.setAdminReason(nurseRequestDto.getAdminReason());
        nurseRequest.setNewDoctorId(nurseRequestDto.getNewDoctorId());
        nurseRequest.setOldDoctorId(nurseRequestDto.getOldDoctorId());
        nurseRequest.setRequestId(nurseRequestDto.getRequestId());

        Nurse nurse=nurseRepository.findById(nurseRequestDto.getNurseId()).get();
        nurseRequest.setNurse(nurse);

        return nurseRequest;
    }
}

