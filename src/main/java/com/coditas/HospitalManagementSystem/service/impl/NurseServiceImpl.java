package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.entity.Role;
import com.coditas.HospitalManagementSystem.entity.User;
//import com.coditas.HospitalManagementSystem.exception.ResourceNotFoundException;
//import com.coditas.HospitalManagementSystem.exception.ResourceNotFoundException;
import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.RoleRepository;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.NurseService;
import com.coditas.HospitalManagementSystem.utils.EmailSenderService;
import com.coditas.HospitalManagementSystem.utils.PasswordEncoderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
@Service
public class NurseServiceImpl implements NurseService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private NurseRepository nurseRepository;

    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private EmailSenderService emailSenderService;
    @Override
    public List<NurseDto> getNurseList() {
        List<Nurse> nurseList=nurseRepository.findAll();
        List<Nurse> nurseList1=nurseList.stream().filter(s-> s.isDeleted()==false).collect(Collectors.toList());
        List<NurseDto> nurseDtoList=nurseList1.stream().map(nurse -> mapToDto(nurse)).collect(Collectors.toList());

        return nurseDtoList;
    }

    @Override
    public NurseDto getSpecificNurse(long nurseId) {
        Nurse nurse=nurseRepository.findById(nurseId).get();
        return mapToDto(nurse);
    }

    @Override
    public List<NurseDto> getAvailableNurseList() {
        List<Nurse> nurseList=nurseRepository.findAll();
        List<Nurse> nurseList1=nurseList.stream().filter(s->s.getStatus().equals("notAllocated")&& s.isDeleted()==false).collect(Collectors.toList());
        List<NurseDto> nurseDtoList=nurseList1.stream().map(nurse -> mapToDto(nurse)).collect(Collectors.toList());

        return nurseDtoList;
    }

    @Override
    public NurseDto addNurse(NurseDto nurseDto) {
        Nurse nurse = mapToEntity(nurseDto);
        Nurse nurseResponse = nurseRepository.save(nurse);

        String password=passWordGenerator();
        String message="Your Nurse Account Has been generated......This is Your PassWord == "+password;
        emailSenderService.sendSimpleEmail(nurse.getEmail(), message,"PassWord");

        User user=new User();
        user.setUserName(nurse.getEmail());
        user.setUserPassword(PasswordEncoderProvider.passwordEncoder(password));
        Role role=roleRepository.findByRoleName("ROLE_NURSE").get();
        user.setRoleSet(Collections.singleton(role));
        userRepository.save(user);

        NurseDto nurseDtoResponse=mapToDto(nurseResponse);
        return nurseDtoResponse;
    }

    @Override
    public NurseDto updateNurse(NurseDto nurseDto, long nurseId) {
        Nurse nurse=nurseRepository.findById(nurseId).get();//.orElseThrow(()->new ResourceNotFoundException("Put","id",nurseId));
        if(nurse!=null) {
            User user=userRepository.findByUserName(nurse.getEmail()).get();

            Nurse nurse1 = mapToEntity(nurseDto);
            nurse1.setNurseId(nurseId);
            Nurse responseNurse= nurseRepository.save(nurse1);

            user.setUserName(responseNurse.getEmail());
            userRepository.save(user);
            NurseDto newDto = mapToDto(nurse1);
            return newDto;
        }
        return null;

    }

    @Override
    public String deleteNurse(long nurseId) {
        Nurse nurse=nurseRepository.findById(nurseId).get();//orElseThrow(()->new ResourceNotFoundException("delete","id",nurseId));
        if(nurse!=null) {
            nurse.setDeleted(true);
            nurse.setDoctor(null);
            nurseRepository.save(nurse);
            return "ok";
        }
        return null;
    }

    @Override
    public NurseDto allocateDoctor(AssignDto assignDto) {
        Doctor doctor=doctorRepository.findById(assignDto.getDoctorId()).get();//orElseThrow(()->new ResourceNotFoundException("post","id",assignDto.getDoctorId()));
        Nurse nurse=nurseRepository.findById(assignDto.getNurseId()).get();//orElseThrow(()->new ResourceNotFoundException("post","id",assignDto.getNurseId()));
        nurse.setDoctor(doctor);
        nurse.setStatus("Allocated");
        Nurse newNurse=nurseRepository.save(nurse);
        return mapToDto(newNurse);
    }

    public NurseDto mapToDto(Nurse nurse){
       NurseDto nurseDto=new NurseDto();
       nurseDto.setNurseId(nurse.getNurseId());
       nurseDto.setEmail(nurse.getEmail());
       nurseDto.setNurseName(nurse.getNurseName());
       nurseDto.setStatus(nurse.getStatus());
       if(nurse.getDoctor()==null){
       }else {
           nurseDto.setDoctorId(nurse.getDoctor().getDoctorId());
           nurseDto.setDoctorName(nurse.getDoctor().getDoctorName());
       }
        return nurseDto;
    }

    public Nurse mapToEntity(NurseDto nurseDto){
        Nurse nurse=new Nurse();
        nurse.setNurseName(nurseDto.getNurseName());
        nurse.setEmail(nurseDto.getEmail());
        return nurse;

    }
    public   String passWordGenerator(){
        Random random=new Random();
        String password= String.valueOf(random.nextInt(100000)+10000);
        return password ;
    }
}
