package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.entity.Role;
import com.coditas.HospitalManagementSystem.entity.User;
import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.RoleRepository;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.DoctorService;
import com.coditas.HospitalManagementSystem.utils.EmailSenderService;
import com.coditas.HospitalManagementSystem.utils.PasswordEncoderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DoctorServiceImpl implements DoctorService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private NurseRepository nurseRepository;
    @Autowired
    private EmailSenderService emailSenderService;
    @Autowired
    private  NurseServiceImpl nurseService;
    @Override
    public List<DoctorDto> getDoctorList() {
        List<Doctor> doctorList=doctorRepository.findAll();
        List<DoctorDto> doctorDtoList=doctorList.stream().filter(d->d.isDeleted()==false).map(doctor -> mapToDto(doctor)).collect(Collectors.toList());

        return doctorDtoList;
    }

    @Override
    public DoctorDto getSpecificDoctor(long doctorId) {
        Doctor doctor=doctorRepository.findById(doctorId).get();
        return mapToDto(doctor);
    }

    @Override
    public DoctorDto addDoctor(DoctorDto doctorDto) {
        Doctor doctor=mapToEntity(doctorDto);

        Doctor doctorResponse=doctorRepository.save(doctor);

        String password=passWordGenerator();
        String message="Your Account Has been generated......This is Your PassWord == "+password;
        emailSenderService.sendSimpleEmail(doctor.getEmail(), message,"PassWord");

        User user=new User();
        user.setUserName(doctorResponse.getEmail());
        user.setUserPassword(PasswordEncoderProvider.passwordEncoder(password));
        Role role=roleRepository.findByRoleName("ROLE_DOCTOR").get();
        user.setRoleSet(Collections.singleton(role));
        userRepository.save(user);

        DoctorDto doctorDtoResponse=mapToDto(doctorResponse);
        return doctorDtoResponse;
    }

    @Override
    public void deleteDoctor(long doctorId) {
        Doctor doctor=doctorRepository.findById(doctorId).get();
        doctor.setDeleted(true);
        for (Nurse n :
                doctor.getNurseSet()) {
            n.setStatus("notAllocated");
            n.setDoctor(null);
        }
        doctorRepository.save(doctor);
    }

    @Override
    public DoctorDto updateDoctor(DoctorDto doctorDto, long doctorId) {
        Doctor doctor=doctorRepository.findById(doctorId).get();
    if(doctor!=null) {
        User user=userRepository.findByUserName(doctor.getEmail()).get();

        Doctor newDoctor = mapToEntity(doctorDto);
        newDoctor.setDoctorId(doctorId);
        Doctor responseDoctor=doctorRepository.save(newDoctor);

        user.setUserName(responseDoctor.getEmail());
        userRepository.save(user);
        DoctorDto newDto = mapToDto(newDoctor);
        return newDto;
    }
    return null;
    }

    @Override
    public DoctorDto allocateNurse(AssignDto assignDto) {
        Doctor doctor=doctorRepository.findById(assignDto.getDoctorId()).get();//orElseThrow(()->new ResourceNotFoundException("post","id",assignDto.getDoctorId()));
        Nurse nurse=nurseRepository.findById(assignDto.getNurseId()).get();//orElseThrow(()->new ResourceNotFoundException("post","id",assignDto.getNurseId()));
        if(nurse.getStatus().equals("notAllocated")){
            Set<Nurse> nurses = doctor.getNurseSet();
            nurses.add(nurse);
            Doctor newDoctor= doctorRepository.save(doctor);
            nurse.setStatus("Allocated");
            nurse.setDoctor(doctor);
            nurseRepository.save(nurse);

           return  mapToDto(newDoctor);

        }
        return null;
    }


    public DoctorDto mapToDto(Doctor doctor){
        DoctorDto doctorDto=new DoctorDto();
        doctorDto.setDoctorId(doctor.getDoctorId());
        doctorDto.setDoctorName(doctor.getDoctorName());
        doctorDto.setEmail(doctor.getEmail());
        if(doctor.getNurseSet()!=null) {
            doctorDto.setNurseSet(doctor.getNurseSet().stream().map(n -> nurseService.mapToDto(n)).collect(Collectors.toSet()));
        }
        doctorDto.setSpecialization(doctor.getSpecialization());

        return doctorDto;
    }

    public Doctor mapToEntity(DoctorDto doctorDto){
        Doctor doctor=new Doctor();
        doctor.setDoctorName(doctorDto.getDoctorName());
        doctor.setDoctorId(doctorDto.getDoctorId());
        doctor.setSpecialization(doctorDto.getSpecialization());
        doctor.setEmail(doctorDto.getEmail());
        return doctor;
    }
    private  String passWordGenerator(){
        Random random=new Random();
        String password= String.valueOf(random.nextInt(100000)+10000);
        return password ;
    }

    public  List<Nurse>  availableNurseList(){
        List<Nurse> nurses=nurseService.getNurseList().stream().map(n->nurseService.mapToEntity(n)).collect(Collectors.toList());
        return nurses;
    }
}
