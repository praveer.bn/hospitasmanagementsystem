package com.coditas.HospitalManagementSystem.service.impl;

import com.coditas.HospitalManagementSystem.entity.User;
import com.coditas.HospitalManagementSystem.exception.ResourceNotFoundException;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import com.coditas.HospitalManagementSystem.service.interfaces.PasswordService;
import com.coditas.HospitalManagementSystem.utils.EmailSenderService;
import com.coditas.HospitalManagementSystem.utils.PasswordEncoderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    NurseServiceImpl nurseService;
    @Autowired
    EmailSenderService emailSenderService;
    @Override
    public String forgotPassWord(String email) {
        User user=userRepository.findByUserName(email).orElseThrow(()->new ResourceNotFoundException("\"Please Enter Existing Mail Id\""));
       if(user!=null) {
           String password = nurseService.passWordGenerator();
           emailSenderService.sendSimpleEmail(email, "your new passWord" + password, "Forgot PassWord");
           user.setUserPassword(PasswordEncoderProvider.passwordEncoder(password));
           userRepository.save(user);
           return "New PassWord Is Sent To Respected Mail Id";
       }
       return "Please Enter Existing Mail Id";
    }

    @Override
    public String resetPassword(String email, String newPassWord,String oldPassWord) {
        User user=userRepository.findByUserName(email).get();
        if(user!=null){

            if( new BCryptPasswordEncoder().matches(oldPassWord,user.getUserPassword())){

                user.setUserPassword(PasswordEncoderProvider.passwordEncoder(newPassWord));
                userRepository.save(user);
                System.out.println("done");
                return "Reset is done";
            }else{
                System.out.println("no");
                return "Old password doesn't match";

            }
        }

        return "Please Enter Existing Mail Id";
    }
}
