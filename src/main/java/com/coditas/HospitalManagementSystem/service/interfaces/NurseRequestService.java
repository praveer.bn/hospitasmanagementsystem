package com.coditas.HospitalManagementSystem.service.interfaces;

import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.payload.NurseRequestDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface NurseRequestService {
    List<NurseRequestDto> getAllRequestHistory();
    List<DoctorDto> getAllAvailableReplacingDoctor(long nurseId);
    List<NurseRequestDto> getSpecificNurseRequestHistory(long nurseId);
    NurseRequestDto doReplaceRequest(NurseRequestDto nurseRequestDto);
}
