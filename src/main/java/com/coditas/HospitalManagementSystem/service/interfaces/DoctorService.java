package com.coditas.HospitalManagementSystem.service.interfaces;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;

import java.util.List;

public interface DoctorService {
    public List<DoctorDto> getDoctorList();
    DoctorDto getSpecificDoctor(long doctorId);
    public DoctorDto addDoctor(DoctorDto doctorDto);
    public DoctorDto updateDoctor(DoctorDto doctorDto,long doctorId);
    public void  deleteDoctor(long doctorId);
    public DoctorDto allocateNurse(AssignDto assignDto);
}
