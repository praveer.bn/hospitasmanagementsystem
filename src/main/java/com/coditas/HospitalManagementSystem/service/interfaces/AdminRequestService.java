package com.coditas.HospitalManagementSystem.service.interfaces;

import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;
import com.coditas.HospitalManagementSystem.payload.NurseRequestDto;
import com.coditas.HospitalManagementSystem.payload.RequestDto;

import javax.mail.search.SearchTerm;
import java.util.List;
import java.util.Set;

public interface AdminRequestService{
    RequestDto nurseRequestHandling(RequestDto requestDto);
    RequestDto doctorRequestHandling(RequestDto requestDto);
    List<DoctorRequestDto> getDoctorPendingRequestHistory();
    List<NurseRequestDto> getNursePendingRequestHistory();
    Set<NurseDto> getNurseForRespectedDoctor(long doctorId);

}
