package com.coditas.HospitalManagementSystem.service.interfaces;

public interface PasswordService {
    String forgotPassWord(String email);
    String resetPassword(String email,String newPassWord,String oldPassWord);
}
