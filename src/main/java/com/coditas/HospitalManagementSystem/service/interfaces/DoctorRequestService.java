package com.coditas.HospitalManagementSystem.service.interfaces;

import com.coditas.HospitalManagementSystem.payload.DoctorRequestDto;

import java.util.List;

public interface DoctorRequestService {
    List<DoctorRequestDto> getAllRequestHistory();
    List<DoctorRequestDto> getSpecificDoctorRequestHistory(long doctorId);
    DoctorRequestDto doReplaceRequest(DoctorRequestDto doctorRequestDto);
}
