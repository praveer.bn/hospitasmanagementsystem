package com.coditas.HospitalManagementSystem.service.interfaces;

import com.coditas.HospitalManagementSystem.payload.AssignDto;
import com.coditas.HospitalManagementSystem.payload.DoctorDto;
import com.coditas.HospitalManagementSystem.payload.NurseDto;

import java.util.List;

public interface NurseService {
    public List<NurseDto> getNurseList();
    NurseDto getSpecificNurse(long nurseId);
    public NurseDto addNurse(NurseDto nurseDto);
    public NurseDto updateNurse(NurseDto nurseDto,long nurseId);
    public String  deleteNurse(long nurseId);
    public NurseDto allocateDoctor(AssignDto assignDto);
    public List<NurseDto> getAvailableNurseList();
}
