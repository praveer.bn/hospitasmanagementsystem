package com.coditas.HospitalManagementSystem.payload;

import com.coditas.HospitalManagementSystem.entity.Nurse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NurseRequestDto {
    @NotEmpty
    private long requestId;
    @NotEmpty
    private long oldDoctorId;
    private String oldDoctorName;
    @NotEmpty
    private long newDoctorId;
    private String newDoctorName;
    @NotEmpty
    private String nurseReason;
    @NotEmpty
    private String adminReason;
    private String requestStatus;
    private String nurseName ;
    private long nurseId;
}






