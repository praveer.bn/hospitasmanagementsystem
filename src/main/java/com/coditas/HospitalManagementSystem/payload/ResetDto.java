package com.coditas.HospitalManagementSystem.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResetDto {
    private String userNameOrEmail;
    private String oldPassword;
    private String newPassword;
}
