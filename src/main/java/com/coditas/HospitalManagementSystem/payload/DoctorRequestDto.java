package com.coditas.HospitalManagementSystem.payload;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorRequestDto {
    @NotEmpty
    private long requestId;

    private long oldNurseId;
    private String oldNurseName;
    @NotEmpty
    private long newNurseId;
    private String newNurseName;
    @NotEmpty
    private String doctorReason;
    @NotEmpty
    private String adminReason;

    private String requestStatus;

    private String doctorName;
    private long doctorId;
}
