package com.coditas.HospitalManagementSystem.payload;

import com.coditas.HospitalManagementSystem.entity.Nurse;
import lombok.*;

import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDto {
    private long doctorId;
    @NotEmpty
    private String doctorName;
    @Email @NotEmpty
    private String email;
    @NotEmpty
    private String specialization;
//    private boolean isDeleted;
    private Set<NurseDto> nurseSet;
}
