package com.coditas.HospitalManagementSystem.payload;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NurseDto {
    private long nurseId;
    @NotEmpty
    private String nurseName;
    @Email @NotEmpty
    private String email;

    private String status;

//    private boolean isDeleted;
    private long doctorId;
    private String doctorName;
}
