package com.coditas.HospitalManagementSystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NurseRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long requestId;
    private long oldDoctorId;
    private long newDoctorId;
    private String nurseReason;
    private String adminReason;
    private String requestStatus="pending";

    @ManyToOne
    private Nurse nurse ;
}
