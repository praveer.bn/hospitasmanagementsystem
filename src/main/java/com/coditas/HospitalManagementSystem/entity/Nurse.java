package com.coditas.HospitalManagementSystem.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "nurse",uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class Nurse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long nurseId;
    private String nurseName;
    private String email;

    private String status="notAllocated";

    private boolean isDeleted=false;
    @ManyToOne
    @JsonManagedReference
    private Doctor doctor;

    @OneToMany(mappedBy = "nurse")
    private List<NurseRequest> nurseRequestList;

}
