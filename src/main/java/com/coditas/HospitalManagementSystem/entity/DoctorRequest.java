package com.coditas.HospitalManagementSystem.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DoctorRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long requestId;
    private long oldNurseId;
    private long newNurseId;
    private String doctorReason;
    private String adminReason;
    private String requestStatus="pending";

    @ManyToOne
    private Doctor doctor;


}
