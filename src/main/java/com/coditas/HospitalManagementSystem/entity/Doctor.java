package com.coditas.HospitalManagementSystem.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "doctor",uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long doctorId;
    private String doctorName;
    private String email;
    private String specialization;

    private boolean isDeleted=false;
    @OneToMany(mappedBy = "doctor")
    @JsonBackReference
    private Set<Nurse> nurseSet;

    @OneToMany(mappedBy = "doctor")
    private List<DoctorRequest> doctorRequestList;
}
