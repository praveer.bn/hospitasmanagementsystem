package com.coditas.HospitalManagementSystem.repository;

import com.coditas.HospitalManagementSystem.entity.NurseRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NurseRequestRepository extends JpaRepository<NurseRequest,Long> {
}
