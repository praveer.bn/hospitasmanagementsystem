package com.coditas.HospitalManagementSystem.repository;

import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NurseRepository extends JpaRepository<Nurse,Long> {
    Optional<Nurse> findByEmail(String email);


}
