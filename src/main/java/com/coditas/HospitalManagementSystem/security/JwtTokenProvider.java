package com.coditas.HospitalManagementSystem.security;


import com.coditas.HospitalManagementSystem.entity.Doctor;
import com.coditas.HospitalManagementSystem.entity.Nurse;
import com.coditas.HospitalManagementSystem.entity.Role;
import com.coditas.HospitalManagementSystem.entity.User;
import com.coditas.HospitalManagementSystem.exception.BlogApiException;
import com.coditas.HospitalManagementSystem.repository.DoctorRepository;
import com.coditas.HospitalManagementSystem.repository.NurseRepository;
import com.coditas.HospitalManagementSystem.repository.UserRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;

@Component
public class JwtTokenProvider {
    @Value("${app.jwt-secret}")
    private String jwtSecret;
    @Value("${app.jwt-expiration-milliseconds}")
    private int jwtExpirationInMs;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private NurseRepository nurseRepository;

    //gen token
    public String generateToken(Authentication authentication){
        String username=authentication.getName();
        Date currentDate=new Date();
        Date expireDate=new Date(currentDate.getTime()+jwtExpirationInMs);

        String token= Jwts.builder().
                setSubject(username).setIssuedAt(new Date()).setExpiration(expireDate).
                signWith(SignatureAlgorithm.HS512,jwtSecret).compact();
                return token;
    }
    //get username
    public String getUsername(String token){
        Claims claims=Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return  claims.getSubject();
    }

    public String getRole(String username){
        User user=userRepository.findByUserName(username).get();
       Set<Role> roleSet= user.getRoleSet();
       String r1 = null;
        for (Role r :
                roleSet) {
            r1=r.getRoleName();
            break;
        }
        String r2=r1.substring(5,r1.length()).toLowerCase();
        return r2;
    }
    public Long getUserId(String username){
        String roleOfUser=getRole(username);
        if(roleOfUser.equals("admin")){
            return 0l;
        } else if (roleOfUser.equals("doctor")) {
            Doctor doctor =doctorRepository.findByEmail(username).get();
            return doctor.getDoctorId();
        }else {
            Nurse nurse=nurseRepository.findByEmail(username).get();
            return nurse.getNurseId();
        }

    }

    //validate
    public boolean getValidate(String token){
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
            return  true;
        }catch (SignatureException ex){
            throw  new BlogApiException(HttpStatus.BAD_REQUEST,"invalid jwt signature");
        }catch (MalformedJwtException ex) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "JWT claims string is empty.");
        }

    }
}
